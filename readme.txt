Admin:
======
1) Go to login page.
	> HTML Page: login.html

2) If user doesn't have an account he/she can create account in Register screen by clicking on Register link
	> HTML Page: register.html
	> For HTML flow purpose I have linked the Register button to dashboard once you click on it will bring you to Dashboard

3) Once logged in you can see dashboard
	> HTML Page: index.html (This will be changed based on the user role once we implement functionality)

4) In Dashboard left side Navigation has Category links.

5) To see Insured data click on the Insured menu item in the left side menu.
	> HTML Page: insured-persons.html

6) To see Uninsured data click on the Uninsured menu item in the left side menu.
	> HTML Page: uninsured-persons.html

7) To see Country Level Information click on Country List link in the left side menu. Once clicked on it you can see the list of countries.
	> HTML Page: country-list.html

8) Each country has Edit, Delete and View options. Admin can edit, delete and view.
	> HTML Pages: 
	> Edit: edit-country.html
	> Delete: There is no screen for delete. Once click on delete pop up window will be opened for asking confirmation, If user confirm to delete that item will be removed from table
	> View: country.html

9) Admin can also add new country details.(Add new country button provided beside the title of the page in right side screen)
	> HTML Page: add-country.html

10) To see the Heat Map for age and income categories click on Reports in left side menu
	> HTML Page: reports.html

12) My account page.
	> HTML Page: my-account.html

13) Logout link.

User Dashboard:
==============
1) User Dashboard has complete information about that country along with categories. 
	> HTML Page: user-dashboard.html

2) The user can update his country details by clicking the edit button provided beside the title of the page in right side screen or the user can click on the Country link left side menu.
	> HTML Page: edit-user-country.html

Wireframes:
==========
All wireframes are there in wireframes folder

HTML Page links:
===============
1. login.hmtl
2. register.html
3. my-account.html
4. add-country.html
5. country-list.html
6. country.html
7. edit-country.html
8. edit-user-country.html
9. index.html
10. insured-persons.html
11. reports.html
12. uninsured-persons.html
13. user-dashboard.html